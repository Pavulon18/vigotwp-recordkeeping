# coding=UTF-8
# MIT License
#
# Copyright (c) 2019-2020 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""    Entry point script for Vigo Twp Recordkeeping.    """


# https://stackoverflow.com/questions/26698628/mvc-design-with-qt-designer-and-pyqt-pyside/26699122#26699122
# I am going to see if I can structure my system following the suggestions
# on this stackexchange answer.

import sys
from PyQt5.QtWidgets import QApplication
from project.classes.model import Model
from project.classes.main_controller import MainController
from project.classes.main_view import MainWindow


class Main(QApplication):

    """    Record keeping application for Vigo Twp Fire Dept.    """

    def __init__(self, sys_argv):
        """        Initializer.        """
        super(Main, self).__init__(sys_argv)
        self.model = Model()
        self.main_controller = MainController(self.model)
        self.main_view = MainWindow(self.model, self.main_controller)
        self.main_view.show()



if __name__ == '__main__':
    vigo_main = Main(sys.argv)  # pylint: disable=invalid-name
    sys.exit(vigo_main.exec_())
