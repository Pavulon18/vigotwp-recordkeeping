# coding=UTF-8
# MIT License
#
# Copyright (c) 2019-2020 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""    Provides the view of the MVC paradigm.    """

from PyQt5.QtWidgets import (QAction,
                                                  QApplication,
                                                  QMainWindow,
                                                  QTabWidget,
                                                  QWidget,
                                                  QLabel,
                                                  QVBoxLayout,
                                                  QFormLayout,
                                                  QHBoxLayout,
                                                  QPushButton,
                                                  QLineEdit,
                                                  QDateEdit,
                                                  QComboBox)
from PyQt5.QtCore import (pyqtSlot,
                                             QObject,
                                             QPoint,
                                             Qt,
                                             QRegExp)
from PyQt5.QtGui import (QRegExpValidator)
from view.admin_view import AdminViewTab


class MainWindow(QMainWindow):

    """    Provides the VIEW of Model-View-Controller.    """

    def __init__(self, model, main_controller):
        """
        Constructor

        @param parent reference to the parent widget
        @type QWidget
        """
        super(MainWindow, self).__init__()
        self._model = model
        self._main_controller = main_controller
        tabs = QTabWidget()
        tabs.setDocumentMode(True)
        tabs.setTabPosition(QTabWidget.North)
        tabs.setMovable(False)



        self.setWindowTitle("Vigo Township Record Keeping")

        self.tab_user_view = QWidget()
        tab_admin_view = AdminViewTab()


        tabs.addTab(self.tab_user_view, 'User Functions')
        tabs.addTab(tab_admin_view, 'Administrator Functions')
        self.user_func_view()
        self.setCentralWidget(tabs)

        # Menu
        self.menu = self.menuBar()
        self.file_menu = self.menu.addMenu("File")

        # Exit QAction
        exit_action = QAction("Exit", self)
        exit_action.setShortcut("Ctrl+Q")
        exit_action.triggered.connect(self.exit_app)

        self.file_menu.addAction(exit_action)

    @pyqtSlot()
    def exit_app(self):
        QApplication.quit()


    def user_func_view(self):
        layout = QHBoxLayout()
        label = QLabel("User View Page")
        layout.addWidget(label)
        self.tab_user_view.setLayout(layout)


















#
#        # connect widgets to controller
#        # See https://stackoverflow.com/questions/26698628/mvc-design-with-qt-designer-and-pyqt-pyside/26699122#26699122
#        # for examples of what to do
##        self._ui.pushButton_admin_func.clicked.connect(self._main_controller.load_admin_func_page)
##        self.button_add_user.clicked.connect(self.on_add_new_user_clicked)
#        self.button_add_user.clicked.connect(self.add_new_user_form)
#
#        # listen for model event signals
##        self._model.enable_admin_func_clicked.connect(self.on_enable_admin_func_clicked)
##        self._model.enable_add_new_user(self.on_enable_add_new_user)
#
#
#

#
##    def admin_func_view(self):
##        # Define the base layout
##        layout_base = QHBoxLayout()
##
##        # Define the layout for the buttons.
##        layout_buttons = QVBoxLayout()
##
##        # I don't recall why I did this.
##        widget_buttons = QWidget()
##
##        # Following the principle above, I will create a blank widget.
##        # This blank widget will then be populated with the forms needed to
##        # complete the admin functions
##        widget_work_area = QWidget()
##
##        # Define the buttons for the administrator functions
##        self.button_add_user = QPushButton('Add New Personnel', self)
##        self.button_update_user = QPushButton('Update Personnel Information')
##        button_list_roster = QPushButton('List Current Roster')
##
##        # Add the buttons to the buttons layout
##        layout_buttons.addWidget(self.button_add_user)
##        layout_buttons.addWidget(self.button_update_user)
##        layout_buttons.addWidget(button_list_roster)
##
##        # add the buttons layout to this widget, for some reason.
##        widget_buttons.setLayout(layout_buttons)
##
##        # Add the widget with the buttons to the base layout
##        layout_base.addWidget(widget_buttons)
##
##        # Add the work area Widget to the base layout
##        layout_base.addWidget(widget_work_area)
#
#        # add the base layout to the admin view tab
#        self.tab_admin_view.setLayout(layout_base)
#
#
##    def on_add_new_user_clicked(self):
##        # I want to make widget_work_area display the add_new_user_form
##        # without adding new columns.
##        layout_base.replaceWidget(self.form_layout)
##
###        self.clear_widget(self.widget_work_area)
##        self.widget_work_area.addWidget(self.add_new_user_form)
##
#
#
#
#
#
#
#
#
#
#
#
#
#
##The click event in PyQt5 is called a signal and the method which gets executed is called a slot.
