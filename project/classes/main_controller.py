# coding=UTF-8
# MIT License
#
# Copyright (c) 2019-20 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
MainController Module.

Provides the controller functions in the MVC paradigm.
This was supposed to be a parent module from
which other controllers could inherit.

"""

from PyQt5.QtCore import QObject, pyqtSlot
from controller.admin_controller import AdminController
from view.admin_view import AdminViewTab



class MainController(QObject):

    """    Provides the controller of Model-View-Controller.    """

    def __init__(self, model):
        """        Initializer.        """
        super().__init__()
        self._model = model

    # @pyqtSlot()
    # See https://stackoverflow.com/questions/26698628/mvc-design-with-qt-designer-and-pyqt-pyside/26699122#26699122
    # for instructions / examples of what to put here.
    # @pyqtSlot()
    def load_admin_func_page(self):
        # self._model.enable_admin_func_page = True if value else False
        self.admin_controller = AdminController(self._model)
        self.admin_view = AdminViewTab(self._model, self.admin_controller)
        self.admin_view.show()
