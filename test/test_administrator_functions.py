# coding=UTF-8
# MIT License
#
# Copyright (c) 2019-2020 The Wise-Baizes Programming Group
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""

Test Module.

Module to test the classes and methods used in the administrator
functions module.

"""

# import pdb; pdb.set_trace()

from project.model.admin_functions import Administrator

import project.classes.vigo_db_setup
from datetime import date
from project.classes.base import session_factory


# global application scope.  create Session
session = session_factory()


person_zero = {
    'first_name': 'Person',
    'last_name': 'Zero',
    'date_of_birth': date(1980, 1, 5),
    'address_line_one': '1313 Mockingbird Lane',
    'zip_code': '84325',
    'username': 'zero',
    'password': 'password'
}

person_one = {
    'first_name': 'Person',
    'last_name': 'One',
    'date_of_birth': date(1980, 1, 5),
    'address_line_one': '1313 Mockingbird Lane',
    'zip_code': '84325',
    'username': 'one',
    'password': 'password'
}

person_two = {
    'first_name':  'Person',
    'last_name':  'Two',
    'date_of_birth': date(1980, 1, 5),
    'address_line_one': '1313 Mockingbird Lane',
    'zip_code': '84325',
    'username': 'two',
    'password': 'password'
}

person_three = {
    'first_name':  'Person',
    'last_name':  'Three',
    'date_of_birth': date(1980, 1, 5),
    'address_line_one': '1313 Mockingbird Lane',
    'zip_code': '84325',
    'username': 'three',
    'password': 'password'
}

person_four = {
    'first_name':  'Person',
    'middle_name': 'Middle',
    'last_name':  'Four',
    'date_of_birth': date(1980, 1, 5),
    'social_security_number': '123456789',
    'address_line_one': '1313 Mockingbird Lane',
    'zip_code': '84325',
    'username': 'four',
    'password': 'password',
    'callsign': '17-100',
    'telephone': '512-867-5309',
    'personnel_email': 'email@example.com',
    'is_password_expired': False,
    'emergency_contact_first_name': 'Frank',
    'emergency_contact_last_name': 'Stein',
    'emergency_contact_telephone': '362-436-5309'
}

person_five = {
    'first_name':  'Person',
    'middle_name': 'Middle',
    'last_name':  'Five',
    'date_of_birth': date(1980, 1, 5),
    'social_security_number': '987654321',
    'address_line_one': '1313 Mockingbird Lane',
    'zip_code': '84325',
    'username': 'five',
    'password': 'password',
    'callsign': '17-100',
    'telephone': '512-867-5309',
    'personnel_email': 'email@example.com',
    'is_password_expired': False,
    'emergency_contact_first_name': 'Frank',
    'emergency_contact_last_name': 'Stein',
    'emergency_contact_telephone': '362-436-5309'
}

class TestAdminFunctions():
    def setUp(self):
        """Setup testing environment."""
        # connect to the database
        # self.connection = engine.connect()

        # begin a non-ORM transaction
        # self.trans = self.connection.begin()

        # bind an individual Session to the connection
        # self.session = Session(bind=self.connection)
        pass

    def test_add_first_personnel_basic_info(self):
        """

        Test the administrator functions model.

        This test will verify that the first person can be added to
        a brand new database without issue.

        """

        new_person = Administrator()
        new_person.add_new_personnel(**person_zero)

    def test_add_multiple_personnel_basic_info(self):
        """
        Verify that more than one person can be added to the database.
        """

        Administrator.add_new_personnel(self, **person_one)

        Administrator.add_new_personnel(self, **person_two)

        Administrator.add_new_personnel(self, **person_three)

    def test_add_personnel_full_info(self):
        """
        This test will add all the information that can go into the
        database for one person.
        """
        Administrator.add_new_personnel(self, **person_four)

    # Update person's info.
    def test_update_personnel_info(self):
        # import pdb; pdb.set_trace()

        # create a personnel record for the test
        Administrator.add_new_personnel(self, **person_five)

        # create information to be updated
        updated_info = {'personnel_email': 'person_five@example.com'}

        old_info = session.query(
            project.classes.vigo_db_setup.Personnel).filter(
            project.classes.vigo_db_setup.Personnel.last_name == "Five").one()
        old_id_num = old_info.personnel_id

        # Send the information to the class / method to be added to the db.
        Administrator.update_personnel_info(self, personnel_id=old_id_num, **updated_info)

        new_info = session.query(project.classes.vigo_db_setup
                                 .Personnel).filter(project.classes.vigo_db_setup
                                 .Personnel.last_name == "Five").one()

        session.refresh(new_info)


        assert new_info.personnel_email == 'person_five@example.com'

    def test_add_new_cert_type(self):
        """Test functionality of add_new_cert_type."""

        imaginary_cert_one = {'certification_name': 'Fake Cert One',
            'issuing_agency': 'Imaginary Agency'}

        Administrator.add_new_cert_type(self, **imaginary_cert_one)

        # session.refresh(new_cert)

        result = session.query(project.classes.vigo_db_setup
            .CertificationType).one()

        assert result.certification_name == 'Fake Cert One'


    # Test attempting to add a duplicate user
        # What criteria should we use to seaarch the DB for dupes?
        # I think I will wait until later to add this functionality.
        # For now, I will just try to get the basic functions working.

    # remove person from active duty
        # Just some thoughts, once a person is added to the personnel,
        # that person will never be deleted.
        #
        # Does this really need testing or is this simply a part of
        # editing a person's record?

    # Read all data about all personnel


    # Test attempting to delete a user not in the system.

    # Test for duplicate usernames

    def tearDown(self):
        self.session.close()

        # rollback - everything that happened with the
        # Session above (including calls to commit())
        # is rolled back.
        self.trans.rollback()

        # return connection to the Engine
        self.connection.close()
        pass

